/*
 * Algebra labs.
 */
package com.example.demo.domain;

import org.junit.Assert;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.example.demo.service.Catalog;

public class UT_Catalog {

    @Test
    public void catalogTest() {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("TODO");
        Assert.assertTrue("spring container should not be null", ctx != null);

        /*
		 * Look up the musicCatalog and invoke its toString method.  
		 * Don't forget to close spring context
         */
    }

}
